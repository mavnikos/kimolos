from modeltranslation.translator import translator, TranslationOptions

from .models import SliderBackground


class SliderBackgroundTranslate(TranslationOptions):
    fields = ("title", )


# class IntroductionTranslate(TranslationOptions):
#     fields = ("title", )
#
#
# class AmenityTranslate(TranslationOptions):
#     fields = ("title", "text")
#
#
# class HomepageSelectTranslate(TranslationOptions):
#     fields = ("title", )


translator.register(SliderBackground, SliderBackgroundTranslate)
# translator.register(Introduction, IntroductionTranslate)
# translator.register(Amenity, AmenityTranslate)
# translator.register(HomepageSelect, HomepageSelectTranslate)
