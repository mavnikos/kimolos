from django.contrib import admin
from django.utils.html import mark_safe
from django.utils.translation import ugettext_lazy as _

from modeltranslation.admin import TranslationAdmin

from .models import SliderBackground, SliderOverlay


class SliderOverlayAdminInline(admin.StackedInline):
    model = SliderOverlay
    fields = (("active", "order"), ("show_background", "background"))
    readonly_fields = ("show_background",)
    min_num = 4
    extra = 0

    def show_background(self, obj):
        if obj.background:
            return mark_safe(f"<img src={obj.background.url} width=100px>")
        return mark_safe(
            _('<span style="color:red;font-weight:bold;">No image</span>')
        )

    show_background.short_description = _("Preview")


@admin.register(SliderBackground)
class HomePagePhotosAdmin(TranslationAdmin):
    list_display = ("show_background", "title")
    readonly_fields = ("show_background",)
    fields = ("title", ("show_background", "background"))
    inlines = [SliderOverlayAdminInline]

    def has_add_permission(self, request):
        return False if self.model.objects.count() > 0 else True

    def show_background(self, obj):
        src = obj.background.url
        return mark_safe(f"<img src={src} width=100px>")

    show_background.short_description = "Slider"
