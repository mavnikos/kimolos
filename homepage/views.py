from smtplib import SMTPException

from django.contrib import messages
from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from django.core.mail import EmailMessage, BadHeaderError
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from contact.forms import ContactForm
from contact.views import email_body
from .models import (
    SliderBackground,
)


def index(request):
    slider = SliderBackground.objects.first()
    overlays = slider.overlays.all()[:4]
    contact_form = ContactForm()
    if request.method == "POST":
        post_data = request.POST
        contact_form = ContactForm(post_data)
        if contact_form.is_valid():
            body = email_body(post_data)
            try:
                email = EmailMessage(
                    subject=f"New message",
                    body=body,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    to=[settings.DEFAULT_TO_EMAIL],
                    reply_to=[post_data.get("email")],
                )
                email.send(fail_silently=True)
            except (BadHeaderError, SMTPException):
                messages.error(
                    request,
                    _("Form error submission. Please try again!"),
                    extra_tags="form_error",
                )
            else:
                messages.success(
                    request,
                    _("Thank you for your message!"),
                    extra_tags="form_success",
                )
                return HttpResponseRedirect(reverse("homepage"))
        else:
            messages.error(
                request,
                _("Form has error(s). Please try again!"),
                extra_tags="form_error",
            )
    context = {
        "slider": slider,
        "overlays": overlays,
        "contact_form": contact_form,
        "page_title": _("Homepage"),
        "meta_description": "Homepage of rantevou kimolos",
    }
    return render(request, "homepage/index.html", context)
