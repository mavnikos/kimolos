from django.contrib import sitemaps
from django.urls import reverse

from room.models import Room


class RoomSitemap(sitemaps.Sitemap):
    changefreq = "yearly"
    priority = 0.6
    i18n = True

    def items(self):
        return Room.objects.all()

    def location(self, obj):
        return obj.get_absolute_url()

    def lastmod(self, obj):
        return obj.updated_at


class StaticViewSitemap(sitemaps.Sitemap):
    changefreq = "yearly"
    priority = 0.5
    i18n = True

    def items(self):
        return [
            "homepage",
            "kimolos",
            "room_index",
            "facilities",
            "gallery",
            "contact",
        ]

    def location(self, obj):
        return reverse(obj)


SITEMAPS = {"room": RoomSitemap, "static": StaticViewSitemap}
