from django.views.generic import TemplateView
from django.urls import path, re_path, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin

from django.contrib.sitemaps.views import sitemap
from django.conf import settings

from .sitemap import SITEMAPS
from homepage import views as homepage_views
from gallery import views as gallery_views
from room import urls as room_urls
from contact import views as contact_views


urlpatterns = [
    path("i18n/", include("django.conf.urls.i18n")),
]

urlpatterns += i18n_patterns(
    path("%s/" % settings.MY_ADMIN_URL, admin.site.urls),
    path("", homepage_views.index, name="homepage"),
    path(
        "kimolos/",
        TemplateView.as_view(template_name="kimolos.html"),
        name="kimolos",
    ),
    path("rooms/", include(room_urls)),
    path(
        "facilities/",
        TemplateView.as_view(template_name="facilities.html"),
        name="facilities",
    ),
    path("gallery/", gallery_views.index, name="gallery"),
    path("contact/", contact_views.index, name="contact"),
)

urlpatterns += [
   re_path(r"^sitemap\.xml/$",
           sitemap,
           {"sitemaps": SITEMAPS, "template_name": "sitemap.xml"},
           name="django.contrib.sitemaps.views.sitemap"
           )
]

if settings.DEBUG:
    from django.views.static import serve
    from django.views.defaults import page_not_found, server_error

    urlpatterns += [
        re_path(
            r"^media/(?P<path>.*)",
            serve,
            {"document_root": settings.MEDIA_ROOT},
        ),
    ]

    urlpatterns += [
        path("404/", page_not_found, {"exception": ""}),
        path("500/", server_error),
    ]
