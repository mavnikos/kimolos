<?php
$subject = 'New Reservation!'; // Subject of your email
$to = 'info@exotheme.com';

$headers = 'From: My Contact Form <'.$to.'>' . "\r\n" . 'Reply-To: ' . $to;
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

$message =  'Name      : ' . $_REQUEST['r_name'] . "\n";
$message .= 'Phone     : ' . $_REQUEST['r_phone'] . "\n";
$message .= 'Email     : ' . $_REQUEST['r_email'] . "\n";
$message .= 'Room Type : ' . $_REQUEST['r_room'] . "\n";
$message .= 'Check In  : ' . $_REQUEST['r_checkin'] . "\n";
$message .= 'Check Out : ' . $_REQUEST['r_checkout'] . "\n";
$message .= 'Adult     : ' . $_REQUEST['r_adult'] . "\n";
$message .= 'Children  : ' . $_REQUEST['r_children'] . "\n";

if (mail($to, $subject, $message, $headers))
{
	// Transfer the value 'sent' to ajax function for showing success message.
	echo 'sent';
}
else
{
	// Transfer the value 'failed' to ajax function for showing error message.
	echo 'failed';
}
?>