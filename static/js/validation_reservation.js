$(document).ready(function () {
	$("#send_book").click(function (e) {
		e.preventDefault();
		var r = !1, a = $("#r_name").val(), n = $("#r_email").val(),
			o = $("#r_phone").val(), _ = $("#r_adult").val(),
			t = ($("#r_children").val(), $("#r_message").val(), $("#r_checkin").val()),
			l = $("#r_checkout").val();
		$("#r_room").val();
		if (0 == a.length) {
			r = !0;
			$("#r_name_error").show()
		} else $("#r_name_error").fadeOut(500);
		if (0 == n.length || "-1" == n.indexOf("@")) {
			r = !0;
			$("#r_email_error").fadeIn(500)
		} else $("#r_email_error").fadeOut(500);
		if (0 == o.length) {
			r = !0;
			$("#r_phone_error").fadeIn(500)
		} else $("#r_phone_error").fadeOut(500);
		if (0 == _.length) {
			r = !0;
			$("#r_guest_error").fadeIn(500)
		} else $("#r_guest_error").fadeOut(500);
		if (0 == t.length) {
			r = !0;
			$("#r_checkin_error").fadeIn(500)
		} else $("#r_checkin_error").fadeOut(500);
		if (0 == l.length) {
			r = !0;
			$("#r_checkout_error").fadeIn(500)
		} else $("#r_checkout_error").fadeOut(500);
		// 0 == r && ($("#send_book").attr({
		// 	disabled: "true",
		// 	value: "Sending..."
		// }), $.post("send_reservation.php", $("#book_form").serialize(), function (e) {
		// 	"sent" == e ? ($("#submit").remove(), $(".notice-success").fadeIn(500)) : ($(".notice-fail").fadeIn(500), $("#send_message").removeAttr("disabled").attr("value", "Send The Message"))
		// }))
	})
});