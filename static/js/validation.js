$(document).ready(function () {
	$("#send_message").click(function (e) {
		e.preventDefault();
		var a = !1, n = $("#name").val(), s = $("#email").val(),
			r = $("#message").val();
		if (0 == n.length) {
			a = !0;
			$("#name_error").fadeIn(500)
		} else $("#name_error").fadeOut(500);
		if (0 == s.length || "-1" == s.indexOf("@")) {
			a = !0;
			$("#email_error").fadeIn(500)
		} else $("#email_error").fadeOut(500);
		if (0 == r.length) {
			a = !0;
			$("#message_error").fadeIn(500)
		} else $("#message_error").fadeOut(500);
		0 == a && ($("#send_message").attr({
			disabled: "true",
			value: "Sending..."
		}), $.post("send_contact.php", $("#contact_form").serialize(), function (e) {
			"sent" == e ? ($("#submit").remove(), $("#mail_success").fadeIn(500)) : ($("#mail_fail").fadeIn(500), $("#send_message").removeAttr("disabled").attr("value", "Send The Message"))
		}))
	})
});