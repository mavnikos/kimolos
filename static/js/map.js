google.maps.event.addDomListener(window, 'load', init);

function init() {
    let myLatlng = new google.maps.LatLng(36.779869, 24.560857);
    let mapOptions = {
        zoom: 16,
        disableDefaultUI: false,
        scrollwheel: false,
        center: myLatlng,
        // How you would like to style the map.
        // This is where you would paste any style found on Snazzy Maps.
        // styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
    };

    // Get the HTML DOM element that will contain your map
    // We are using a div with id="map" seen below in the <body>
    let mapElement = document.getElementById('map');

    // Create the Google Map using out element and options defined above
    let map = new google.maps.Map(mapElement, mapOptions);

    let marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Lorem Ipsum'
    });
}
