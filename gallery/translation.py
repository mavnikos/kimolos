from modeltranslation.translator import translator, TranslationOptions

from .models import MasonryImage


class MasonryImageTranslate(TranslationOptions):
    fields = ("name", )


translator.register(MasonryImage, MasonryImageTranslate)
