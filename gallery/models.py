from django.db import models
from django.utils.translation import ugettext_lazy as _


class MasonryImage(models.Model):
    image = models.ImageField(
        _("Image"),
        upload_to="gallery",
        help_text=_("Image file size less than 350KB."),
    )
    name = models.CharField(_("Name"), max_length=100, blank=True)

    class Meta:
        verbose_name = _("Gallery")
        verbose_name_plural = _("Galleries")

    def __str__(self):
        return self.name or str(self.pk)
