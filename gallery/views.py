from django.shortcuts import render
# from django.utils.translation import ugettext_lazy as _

from .models import MasonryImage


def index(request):
    images = MasonryImage.objects.all()
    return render(
        request,
        "gallery.html",
        {
            "images": images,
            "meta_description": "Media gallery of rantevou kimolos.",
        },
    )
