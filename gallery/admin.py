from django.contrib import admin
from django.utils.html import mark_safe

from modeltranslation.admin import TranslationAdmin

from .models import MasonryImage


@admin.register(MasonryImage)
class MasonryGalleryAdmin(TranslationAdmin):
    list_display = ("show_thumb_admin", "name", "image")
    fields = ("show_thumb_admin", "image", "name")
    readonly_fields = ("show_thumb_admin",)

    def show_thumb_admin(self, obj):
        if obj.image:
            src = obj.image.url
            return mark_safe(f"<img src={src} width=100px>")
        return mark_safe(
            '<span style="color:red;font-weight:bold;">No image</span>'
        )
