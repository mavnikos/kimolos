from django.db import models
from django.urls import reverse_lazy
from django.utils.text import slugify
from django.utils.translation import override
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField


class RoomAmenity(models.Model):
    name = models.CharField(max_length=300)
    icon_name = models.CharField(
        max_length=300,
        default="check-circle",
        help_text=_(
            "Pick a name of this amenity from "
            "<a target='_blank' href='https://fontawesome.com/v4.7.0/icons/'>this link</a>."
        ),
    )
    description = models.TextField(
        _("Description"), help_text=_("Short description of this amenity."),
        null=True, blank=True
    )
    order = models.PositiveSmallIntegerField(default=0)

    class Meta:
        verbose_name = _("amenity")
        verbose_name_plural = _("amenities")
        ordering = ["order"]

    def __str__(self):
        return self.name


class Room(models.Model):
    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(unique=True)
    amenities = models.ManyToManyField(RoomAmenity, blank=True)
    short_description = models.TextField(
        help_text=_("Description that appears in the room index page.")
    )
    description = RichTextField(_("Description"), null=True, blank=True)
    top_image = models.ImageField(
        _("Top image"),
        upload_to="top_images",
        help_text=_(
            "Select image for the top of the page.<br>"
            "Max dimensions: <strong>1920x300px</strong>.<br>"
        ),
    )
    order = models.SmallIntegerField(
        _("Order of appearance"),
        blank=True,
        null=True,
    )
    meta_description = models.TextField(max_length=160)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    class Meta:
        verbose_name = _("room")
        verbose_name_plural = _("rooms")
        ordering = ["order"]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        with override(language="en"):
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse_lazy("room_detail", kwargs={"room_slug": self.slug})


class RoomImageDeposit(models.Model):
    room = models.ForeignKey(
        Room,
        verbose_name=_("Room"),
        related_name="images",
        on_delete=models.CASCADE,
    )
    large_img = models.ImageField(
        _("Large image"),
        upload_to="room",
        help_text=_(
            "Select room's large image.<br>"
            "Optimum dimensions: <strong>800x500px</strong>.<br>"
        ),
    )
    order = models.SmallIntegerField(
        _("Order of appearance"),
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _("room's large image")
        verbose_name_plural = _("room's large images")
        ordering = ["order", "room__name"]

    def __str__(self):
        return self.room.name


# class RoomIndex(models.Model):
#     top_image = models.ImageField(
#         help_text=_(
#             "Select image for the top of the page.<br>"
#             "Max dimensions: <strong>1920x300px</strong>.<br>"
#         )
#     )
#
#     class Meta:
#         verbose_name = _("top image")
#         verbose_name_plural = _("top image")
