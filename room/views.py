from smtplib import SMTPException

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.core.mail import EmailMessage, BadHeaderError
from django.conf import settings

from jinja2.exceptions import TemplateNotFound

from .models import Room
from .forms import BookingForm


def get_template(template_name="room_email_body.txt"):
    template = None
    env = settings.GET_JINJA_ENV()
    try:
        template = env.get_template(template_name)
    except TemplateNotFound:
        pass
    return template


def email_body(data):
    template = get_template()
    context = {"data": data}
    body = template.render(**context)
    return body


def index(request):
    rooms = Room.objects.all()
    context = {
        "rooms": rooms,
        "page_title": _("Rooms"),
        "meta_description": "Available rooms of rantevou kimolos",
    }
    return render(request, "room/index.html", context)


def detail(request, room_slug):
    booking_form = BookingForm()
    room = get_object_or_404(Room, slug=room_slug)
    if request.method == "POST":
        post_data = request.POST
        booking_form = BookingForm(post_data)
        if booking_form.is_valid():
            body = email_body(post_data)
            try:
                email = EmailMessage(
                    subject=f"Message for {post_data.get('room_name')}",
                    body=body,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    to=[settings.DEFAULT_TO_EMAIL],
                    reply_to=[post_data.get("email")],
                )
                email.send(fail_silently=True)
            except (BadHeaderError, SMTPException):
                messages.error(
                    request,
                    _("Form error submission. Please try again!"),
                    extra_tags="form_error",
                )
            else:
                messages.success(
                    request,
                    _("Thank you for your message!"),
                    extra_tags="form_success",
                )
                return HttpResponseRedirect(room.get_absolute_url())
        else:
            messages.error(
                request,
                _("Form has error(s). Please try again!"),
                extra_tags="form_error",
            )
    return render(
        request,
        "room/detail.html",
        {"room": room, "booking_form": booking_form},
    )
