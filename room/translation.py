from modeltranslation.translator import translator, TranslationOptions

from .models import Room, RoomAmenity


class RoomAmenityTranslate(TranslationOptions):
    fields = ("name", "description")


class RoomTranslate(TranslationOptions):
    fields = (
        "name",
        "short_description",
        "description",
        "meta_description",
    )


translator.register(Room, RoomTranslate)
translator.register(RoomAmenity, RoomAmenityTranslate)
