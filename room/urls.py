from django.urls import path

from .views import index, detail


urlpatterns = [
    path("", index, name="room_index"),
    path("<slug:room_slug>/", detail, name="room_detail"),
]
