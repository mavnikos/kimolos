from datetime import datetime

from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import TextInput
from django import forms

DATE_FORMAT = "%Y-%m-%d"


class TelInput(TextInput):
    input_type = "tel"


class BookingForm(forms.Form):
    ADULTS_CHOICES = (
        ("1", "1"),
        ("2", "2"),
        ("3", "3"),
        ("4", "4"),
        ("5", "5"),
        ("5+", "5+"),
    )
    CHILDREN_CHOICES = (
        ("0", "0"),
        ("1", "1"),
        ("2", "2"),
        ("3", "3"),
        ("4", "4"),
        ("5", "5"),
    )

    checkin_date = forms.CharField(
        max_length=10,
        required=True,
        label=_("Check In Date"),
        label_suffix="",
        widget=forms.TextInput(
            attrs={"class": "form-control", "id": "r_checkin"}
        ),
    )
    checkout_date = forms.CharField(
        max_length=10,
        required=True,
        label=_("Check Out Date"),
        label_suffix="",
        widget=forms.TextInput(
            attrs={"class": "form-control", "id": "r_checkout"}
        ),
    )
    name = forms.CharField(
        max_length=90,
        required=True,
        label=_("Name"),
        label_suffix="",
        widget=forms.TextInput(
            attrs={"class": "form-control", "id": "r_name"}
        ),
    )
    adults = forms.ChoiceField(
        choices=ADULTS_CHOICES,
        initial=ADULTS_CHOICES[0][1],
        label=_("Adults"),
        label_suffix="",
        widget=forms.Select(
            attrs={"class": "form-control", "id": "r_adult"}
        ),
    )
    children = forms.ChoiceField(
        choices=CHILDREN_CHOICES,
        initial=CHILDREN_CHOICES[0][0],
        label=_("Children"),
        label_suffix="",
        widget=forms.Select(
            attrs={"class": "form-control", "id": "r_children"}
        ),
    )
    phone = forms.CharField(
        max_length=40,
        label=_("Phone"),
        label_suffix="",
        widget=TelInput(
            attrs={"class": "form-control", "id": "r_phone"}
        ),
    )
    email = forms.EmailField(
        label="Email",
        label_suffix="",
        error_messages={"invalid": _("Invalid email")},
        widget=forms.EmailInput(
            attrs={"class": "form-control", "id": "r_email"}
        ),
    )
    message = forms.CharField(
        label=_("Message"),
        label_suffix="",
        required=False,
        widget=forms.Textarea(
            attrs={"class": "form-control", "id": "r_message", "rows": "6"}
        ),
    )

    def clean_checkin_date(self):
        data = self.cleaned_data["checkin_date"]
        try:
            datetime.strptime(data, DATE_FORMAT)
        except ValueError:
            raise forms.ValidationError(_("Invalid date!"))
        return data

    def clean_checkout_date(self):
        data = self.cleaned_data["checkout_date"]
        try:
            datetime.strptime(data, DATE_FORMAT)
        except ValueError:
            raise forms.ValidationError(_("Invalid date!"))
        return data
